SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE `announcements`;
TRUNCATE `api_tokens`;
TRUNCATE `invitations`;
TRUNCATE `invoices`;
TRUNCATE `messages`;
TRUNCATE `notifications`;
TRUNCATE `messages`;
TRUNCATE `password_resets`;
TRUNCATE `performance_indicators`;
TRUNCATE `profiles`;
TRUNCATE `sentiments`;
TRUNCATE `stats`;
TRUNCATE `subscriptions`;
TRUNCATE `team_subscriptions`;
TRUNCATE `team_users`;
TRUNCATE `teams`;
TRUNCATE `users`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `teams` (`id`, `owner_id`, `name`, `slug`, `photo_url`, `stripe_id`, `current_billing_plan`, `card_brand`, `card_last_four`, `card_country`, `billing_address`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `vat_id`, `extra_billing_information`, `trial_ends_at`, `created_at`, `updated_at`, `slack_id`)
VALUES
	(1,22,'Test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','2017-01-07 13:16:11','2016-12-28 13:16:11','2016-12-28 13:16:11','T02FDBC3H');

INSERT INTO `profiles` (`id`, `chat_id`, `username`, `first_name`, `last_name`, `avatar`, `email`, `is_admin`, `is_owner`, `is_restricted`, `is_bot`, `team_id`, `created_at`, `updated_at`)
VALUES
	(9, 'U02FDFW7B', 'richard', 'Richard', 'Edwards', '/img/richard.png', 'garethe@oneafricamedia.com', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:24:52', '2017-01-06 07:24:52'),
	(10, 'U02SB9540', 'dinesh', 'Dinesh', 'Forbes', '/img/dinesh.png', 'marlinf@datashaman.com', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:30:59', '2017-01-06 07:30:59'),
	(11, 'U02FDBC3P', 'erlich', 'Erlich', 'Körber', '/img/erlich.png', 'nilsk@oneafricamedia.com', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:33:19', '2017-01-06 07:33:19'),
	(12, 'U03S6Q0AV', 'gilfoyle', 'Gilfoyle', 'Motjolopane', '/img/gilfoyle.png', 'thatom@oneafricamedia.com', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:34:26', '2017-01-06 07:34:26'),
	(13, 'U047G1XTA', 'jared', 'Jared', 'Wild', '/img/jared.png', 'ryanw@oneafricamedia.com', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:35:52', '2017-01-06 07:35:52'),
	(14, 'U02KGU25G', 'bighead', 'Bighead', 'Kithika', '/img/bighead.png', 'charlesk@cheki.co.ke', NULL, NULL, NULL, NULL, 1, '2017-01-06 07:45:14', '2017-01-06 07:45:14');

INSERT INTO `users` (`id`, `name`, `email`, `password`, `social_token`, `remember_token`, `photo_url`, `uses_two_factor_auth`, `authy_id`, `country_code`, `phone`, `two_factor_reset_code`, `current_team_id`, `stripe_id`, `current_billing_plan`, `card_brand`, `card_last_four`, `card_country`, `billing_address`, `billing_address_line_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `vat_id`, `extra_billing_information`, `trial_ends_at`, `last_read_announcements_at`, `created_at`, `updated_at`)
VALUES
	(2,'bbbb','bbbb@gmail.com','$2y$10$VOY5xwYhHTgz.rqj65TIZ.h50WgMHf5hmZtS6AQ.LIAbnTloXxT92',NULL,'4qLqa6SJMcr2z3uNlyItvS1mUFMl5K7ZNxZso061xVVTyYhrcvmH86XUBU5d',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-07 12:46:49','2016-12-28 12:46:49','2016-12-28 12:46:49','2016-12-28 12:47:16'),
	(19,'Ms. Reva DuBuque','rafaela20@example.net','$2y$10$caA0uCyPyTaBGMo7GT3Xf.10lTTOkZj7OkG/xIYHEp8LiAPJVaTyy',NULL,'DFOIJO3ISh',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-31 11:18:59','2016-12-31 11:18:59'),
	(20,'Vito Marvin','maynard75@example.net','$2y$10$ndFqGvPDkQx0xgKvLdeZ8OTZ/fLdtmZn6BPjrFRC/x68xpJqOVfb.',NULL,'staZA4CbOJ',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-31 11:18:59','2016-12-31 11:18:59'),
	(21,'Lemuel Lemke','dkreiger@example.org','$2y$10$hzSAHb1nQ0MQUSJIxkLIeebU18/vQsK65I55bw7R.89ESG.v0i8F.',NULL,'H7KtU7EuMR',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-31 11:19:18','2016-12-31 11:19:18'),
	(22,'Raul Spinka','cmueller@example.org','$2y$10$0ZNmWgPLY92E5vVr7uueN.gdDmW3fe6bbDBipDyAHa83jbpyf/Wqq',NULL,'sS8oweZZho',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-31 11:19:18','2016-12-31 11:19:18');
