<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Message::class, function (Faker\Generator $faker) {
    $profile = \App\Profile::inRandomOrder()->first();

    return [
        'channel' => $faker->slug(),
        'user' => $profile->chat_id,
        'text' => $faker->text(),
        'team_id' => 1,
        'event' => 'ambient',
        'ts' => $faker->randomFloat(),
        'created_at' => $faker->dateTimeBetween('-1 year'),
    ];
});

$factory->define(App\Team::class, function (Faker\Generator $faker) {

    return [
        'owner_id' => 1,
        'name' => $faker->name,
        'slack_id' => $faker->slug(),
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    $team = factory(\App\Team::class)->create();

    return [
        'avatar' => $faker->image(),
        'chat_id' => $faker->slug(),
        'email' => $faker->email(),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'team_id' => $team->id,
        'username' => $faker->userName,
    ];
});

$factory->define(App\Sentiment::class, function (Faker\Generator $faker) {
    $message = factory(\App\Message::class)->create();
    $words = [
        'click-farming',
        'aviato',
        'hooli',
        'bachmanity',
        "dinesh's code",
        "gilfoyle's code",
        'parking',
        'kitchen',
        'stand-up',
        'weather',
        'trump',
        'big head',
        'middle-Out compression ',
        '@U02FDFW7B',
        '@U02SB9540',
        '@U02FDBC3P',
        '@U03S6Q0AV',
        '@U047G1XTA',
        '@U02KGU25G',
        '@U03D9AK86',
    ];

    $key = array_rand($words);

    return [
        'message_id' => $message->id,
        'sentiment' => $faker->randomFloat(null, -1, 1),
        'entities' => json_encode([$words[$key]]),
        'created_at' => $message->created_at,
    ];
});

