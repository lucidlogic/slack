<?php

use Illuminate\Database\Seeder;

class StatsSeeder extends Seeder
{

    public function run()
    {
        factory(\App\Sentiment::class)->times(3000)->create();
    }
}
