@if(isset($entityStats))
    <div class="panel-body" data-intro="{{ trans('intro.entities') }}" data-step="2">
        <h3>{{ isset($profile) ?  $profile->first_name . "'s" : "Your Teams's" }} Topics</h3>
        @foreach($entityStats as $stat)
            <a
                href="{{ route(
                            'user.dashboard.entity',
                            [
                                'entity' => urlencode($stat->entity),
                                'interval' => request()->input('interval')
                            ]
                         )
                         }}"
                class="btn btn-primary entities {{ str_slug(friendlySentiment($stat->avg_sentiment)) }}"
                data-toggle="popover"
                data-placement="left"
                title="sentiment"
                data-content="{{ large_emoji($stat->avg_sentiment) }} The sentiment about the topic <strong>'{{ friendlyEntity($stat->entity) }}'</strong>
                                                was on average <strong>{!! friendlySentiment($stat->avg_sentiment) !!}</strong>
                                                with a sentiment score of </i>{{ round($stat->avg_sentiment, 2) }}</i> <br />
                                                total messages: (<strong>{{ $stat->message_count }}</strong>)"
                data-trigger="hover"
            >
                {!! friendlyEntity($stat->entity) !!}
            </a>
        @endforeach
    </div>
@endif
