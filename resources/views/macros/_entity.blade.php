@if(isset($entity))
    <div class="panel-body">
        <h2 class="entity_heading"><br><span class="entity">{!! friendlyEntity($entity) !!}</span></h2>
        <div class="lead" style="float:left">
            <p>&nbsp;</p>
            total message: {{ count($lineData['points']) }} <br>
            team members with opinions: {{ count($profileStats) }}<br>
            overall sentiment: {{ friendlySentiment($sentiment) }}
        </div>
        <div class="entity_emoji">{{ sentient_emoji($sentiment) }}</div>
    </div>
@endif

