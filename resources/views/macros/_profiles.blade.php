@if(isset($profileStats))
    <div class="panel-body" data-intro="{{ trans('intro.profiles') }}" data-step="3">
        <p class="bg-info"><h3>Team members ({{ count($profileStats) }}) @if(isset($entity)) talking about <em>'{!! friendlyEntity($entity) !!}'</em> @endif</h3></p>
        @foreach($profileStats as $stat)
            <a href="{{ route(
                            'user.dashboard.profile',
                            [
                                'profile' => $stat->profile->username,
                                'interval' => request()->input('interval')
                            ]
                         )
                   }}">
                <img
                    src="{!! $stat->profile->avatar !!}"
                    width="200px"
                    class="spark-nav-profile-photo-l m-r-m"
                    data-placement="left"
                    data-toggle="popover"
                    data-content="{{ large_emoji($stat->avg_sentiment) }} sentiment: <strong>{!! friendlySentiment($stat->avg_sentiment) !!}</strong> <br />
                                            <i>{{ round($stat->avg_sentiment, 3) }}</i> <br />
                                           total messages: (<strong>{{ $stat->message_count }}</strong>)"
                    title="{!! $stat->profile->full_name !!}"
                    data-trigger="hover"
                />
            </a>
        @endforeach
    </div>
@endif
