@if(isset($profile))
    <img
            src="{!! $profile->avatar !!}"
            width="200px"
            class="spark-nav-profile-photo-l m-r-m"
            data-placement="bottom"
            title="Profile"
            data-toggle="popover"
            data-content="{!! $profile->full_name !!}"
            data-trigger="hover"
            style="float: left"
    />
    @include('macros._entities')
@endif
