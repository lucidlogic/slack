@if(count($lineData['points'])>1)
    <div class="panel-body" >
        <h3 style="text-align: center">{{ array_get($lineData, 'title') }}</h3>
        <p>&nbsp;</p>
        <canvas id="sentimentLine" data-intro="{{ trans('intro.lineGraph') }}" data-step="1"></canvas>
    </div>
@endif
