@extends('spark::layouts.app')
@section('title')
    {{ config('app.name') }} | Sentiments in Slack
@endsection
@section('content')
    <div class="home_heading">Sentiment Analysis for Slack <a href="{{ route('slack.redirect') }}"></a><br/>
        {{ slack_button() }}
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body features">
                        <div class="row" id="features">
                            <div class="col-sm-6 col-md-6">
                                <div class="thumbnail">
                                    <img src="/img/features/line.png" class="feature_image thumbnail" alt="Mood" />
                                    <div class="caption">
                                        <h3 style="margin-top: 36px;">Sentiment</h3>
                                        <p>See the general sentiment of your team over time. <br />
                                        With weekly, monthly & yearly options
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="thumbnail">
                                    <img src="/img/features/topic.png" class="feature_image thumbnail" alt="Topics" style="margin-bottom: 10px;"/>
                                    <div class="caption">
                                        <h3>Topics</h3>
                                        <p>See the sentiment towards certain topics.<br/> Example: <em>Danesh really
                                                hates the parking.</em></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="thumbnail">
                                    <img src="/img/features/profile.png" class="feature_image thumbnail" alt="Members"/>
                                    <div class="caption">
                                        <h3>Team Members</h3>
                                        <p>See the general mood of team members.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 bottom_padding">
                                <div class="thumbnail">
                                    <img src="/img/features/bully.png" class="feature_image thumbnail"
                                         alt="Bully Warning"/>
                                    <div class="caption">
                                        <h3>Bully Warning</h3>
                                        <p>Get notified when extremely negative messages are being said about a team
                                            member.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body pricing top_padding" id="pricing" style="padding-top:0px">
                        <div class="alert alert-success" role="alert">
                            <h1>Pricing</h1>
                            <p>Add Mood Climate to your public channels and get unlimited sentiment reports for $19/mo.
                                <br/>
                                <strong>Start your 14 day free trial today!</strong>
                            </p>
                        </div>
                        {{ slack_button() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
