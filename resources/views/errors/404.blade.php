@extends('spark::layouts.app')
@section('title')
    {{ config('app.name') }} | Umm... this is awkward
@endsection
@section('content')
    <div class="home_heading">Sentiment Analysis for Slack <a href="{{ route('slack.redirect') }}"><img
                    src="/img/slack-logo.png" width="50px"></a><br/>
        {{ slack_button() }}
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="alert alert-success" role="alert">
                            <h1>Page not found </h1>
                            <p>Sometimes it helps to check to make sure you didn't misspell the URL. If that doesn't
                                work you can head back to the <a href="/">homepage</a>. If you think you're right and we
                                are messing something up, <a href="mailto:info@moodclimate.co">shoot us an email.</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
