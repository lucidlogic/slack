@extends('spark::layouts.app')
@section('title')
    {{ config('app.name') }} | Umm... this is awkward
@endsection
@section('content')
    <div class="home_heading">Sentiment Analysis for Slack <a href="{{ route('slack.redirect') }}"><img
                    src="/img/slack-logo.png" width="50px"></a><br/>
        {{ slack_button() }}
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body" style="text-align: center">
                        <div class="alert alert-danger" role="alert">
                            <h1>Well, this is awkward</h1>
                            <p>There seems to be a problem :(  Please go back to the <a href="/">homepage</a> and try again. Feel free to <a href="mailto:info@moodclimate.co">shoot us an email.</a> and vent your anger
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
