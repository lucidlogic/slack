@extends('spark::layouts.app')
@section('title')
    {{ config('app.name') }} | Dashboard
@endsection
@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h1>I need more messages 😫</h1></div>
                        <div class="panel-body">
                            <p>Please add <a hre="{{ team_url() }}">@mood_climate</a> to your slack channels (the more the better!) so we can start analysing </p>
                            <ul>
                                <li class="steps"><img src="/img/steps/1.png" width="300px" class="box-shadow"></li>
                                <li class="steps"><img src="/img/steps/2.png" width="400px" class="box-shadow"></li>
                                <li class="steps"><img src="/img/steps/4.png" width="500px" class="box-shadow"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </home>
@endsection
