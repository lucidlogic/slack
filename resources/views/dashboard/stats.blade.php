@extends('spark::layouts.app')
@section('title')
    {{ config('app.name') }} | Dashboard
@endsection
@section('content')
    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        @include('macros._profile')
                        @include('macros._entity')
                        @include('macros._line')
                        @include('macros._profiles')
                        @unless(isset($profile))
                            @include('macros._entities')
                        @endunless
                    </div>
                </div>
            </div>
        </div>
    </home>
@endsection
@section('scripts')
    <style>
        .entity_emoji{
            float: right;
            font-size: 100px;
        }
        .entity_heading{
            float: left;
            margin-right: 50px;
        }
        .entity{
            border: 2px solid #d3e0e9;
            border-radius: 50%;
            padding: 10px;
        }
        .large_emoji {
            font-size: 100px;
            opacity: 0.8;
        }

        .entities {
            margin: 10px;
        }

        .spark-nav-profile-photo-l {
            border: 2px solid #d3e0e9;
            border-radius: 68%;
            padding: 2px;
            height: 200px;
            width: 200px;
            margin: 10px;
        }

        .extremely-positive {
            background-color: #19c605;
        }

        .very-positive {
            background-color: #1fad0f;
        }

        .positive {
            background-color: #cae5c7;
        }

        .neutral {
            background-color: #f1ffef;
        }

        .negative {
            background-color: #f9d9d9;
        }

        .very-negative {
            background-color: #f44242;
        }

        .extremely-negative {
            background-color: #6d6868;
        }
    </style>
    <?php
            ?>
    <script src="/node_modules/chart.js/dist/Chart.js"></script>
    <script>
        window.onload = function () {
                    @if(count($lineData['points'])>1)

            var data = {
                        labels: ['{!! implode("','", $lineData['labels']) !!}'],
                        datasets: [
                            {
                                label: '',
                                fill: false,
                                lineTension: 0.1,
                                backgroundColor: "rgba(75,192,192,0.4)",
                                borderColor: "#7a23b4",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "#7a23b4",
                                pointBackgroundColor: "#fff",
                                pointBorderWidth: 1,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#7a23b4",
                                pointHoverBorderColor: "#7a23b4",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 10,
                                data: [{!! implode(',', $lineData['points']) !!}],
                                spanGaps: false,
                                showLines: true
                            }
                        ]
                    };
            var ctx = document.getElementById("sentimentLine");
            var sentimentLine = new Chart(ctx, {
                type: 'line',
                data: data,
                options: {
                    scales: {
                        xAxes: [{
                            display: true
                        }]
                    },
                    title: {
                        display: false,
                        text: '{{ array_get($lineData, 'title') }}'
                    },
                    legend: {
                        display: false,
                    }
                }
            });
            @endif
                    $('[data-toggle="popover"]').popover({
                html: true
            });
        }
    </script>
@endsection
