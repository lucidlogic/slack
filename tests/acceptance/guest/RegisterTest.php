<?php

use App\tests\TestCase;
use App\User;

class RegisterTest extends TestCase
{
    public function testSlackRegister()
    {
        Socialite::shouldReceive('driver->redirect')->andReturn(
            redirect()->route('slack.login')
        );

        $socialUser = $this->mocksocialUser();

        Socialite::shouldReceive('driver->user')->andReturn(
            $socialUser
        );

        $this
            ->visit('/')
            ->click('socialLogin')
            ->assertResponseOk()
            ->seeInDatabase(
                'users',
                [
                    'email' => $socialUser->email,
                    'social_token' => $socialUser->token,
                ]
            )
            ->seeInDatabase(
                'teams',
                [
                    'name' => $socialUser->user['team']['name'],
                ]
            );
        $user = User::where('social_token', $socialUser->token)->first();
        $team = \App\Team::whereSlackId($socialUser->organization_id)->first();
        $this
            ->seeInDatabase(
                'notifications',
                [
                    'user_id' => $user->id
                ]
            );
        $this->assertTrue($user->ownsTeam($team));
    }

    public function testSlackRegisterExistingTeam()
    {
        Socialite::shouldReceive('driver->redirect')->andReturn(
            redirect()->route('slack.login')
        );

        $socialUser = $this->mocksocialUser();

        $team = factory(\App\Team::class)->create();

        $socialUser->organization_id = $team->slack_id;

        Socialite::shouldReceive('driver->user')->andReturn(
            $socialUser
        );

        $this
            ->visit('/')
            ->click('socialLogin')
            ->assertResponseOk()
            ->seeInDatabase(
                'users',
                [
                    'email' => $socialUser->email,
                    'social_token' => $socialUser->token,
                ]
            );
        $user = User::where('social_token', $socialUser->token)->first();
        $team = \App\Team::whereSlackId($socialUser->organization_id)->first();
        $this->assertEquals('member', $user->roleOn($team));
    }

    /**
     * @return Laravel\Socialite\Two\User
     */
    protected function mocksocialUser()
    {
        $socialUser = app(\Laravel\Socialite\Two\User::class);
        $faker = app(Faker\Generator::class);

        $socialUser->token = str_slug($faker->catchPhrase);
        $socialUser->expiresIn = $faker->dateTimeThisMonth;
        $socialUser->name = $faker->name;
        $socialUser->email = $faker->email;
        $socialUser->id = $faker->uuid;
        $socialUser->user = [
            'name' => $faker->name,
            'first_name' => $faker->firstname,
            'last_name' => $faker->lastname,
            'gender' => 'female',
            'verified' => true,
            'team' => [
                'name' => $faker->catchPhrase,
                'domain' => str_slug($faker->catchPhrase),
            ]
        ];
        $socialUser->organization_id = str_slug($faker->catchPhrase);;

        return $socialUser;
    }
}
