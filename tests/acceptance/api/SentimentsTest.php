<?php

use App\Sentiment;
use App\tests\TestCase;

class SentimentsTest extends TestCase
{
    public function testStore()
    {
        $token = $this->testToken();
        $message = factory(\App\Message::class)->create();

        $data = [
            'message_id' => $message->id,
            'sentiment' => 0.5,
        ];

        $this
            ->post(
                route('api.sentiments.storeOrUpdate'),
                $data + [
                    'api_token' => $token->token,
                ]
            )
            ->assertResponseOk()
            ->seeInDatabase(
                'sentiments',
                $data
            );

        $data['sentiment'] = 0.7;
        $this
            ->post(
                route('api.sentiments.storeOrUpdate'),
                $data + [
                    'api_token' => $token->token,
                ]
            )
            ->assertResponseOk()
            ->seeInDatabase(
                'sentiments',
                $data
            );
    }

    public function testBullyWarning()
    {
        $token = $this->testToken();
        $message = factory(\App\Message::class)->create();

        $data = [
            'message_id' => $message->id,
            'sentiment' => -1,
            'entities' => json_encode([
               '@' .  \App\Profile::first()->chat_id,
            ]),
        ];

        $this
            ->post(
                route('api.sentiments.storeOrUpdate'),
                $data + [
                    'api_token' => $token->token,
                ]
            )
            ->assertResponseOk();
        $sentiment = Sentiment::whereMessageId($message->id)->first();
        dd(json_decode($sentiment->entities));
        $this
            ->seeInDatabase(
                'notifications',
                [
                    'user_id' => $message->team->owner_id,
                ]
            );
    }
}
