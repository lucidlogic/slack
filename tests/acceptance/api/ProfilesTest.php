<?php

use App\Team;
use App\tests\TestCase;

class ProfilesTest extends TestCase
{
    public function testStore()
    {
        $token = $this->testToken();
        $team = factory(\App\Team::class)->create();

        $data = [
            'chat_id' => 'test',
            'username' => 'test',
            'first_name' => 'test',
            'last_name' => 'test',
            'avatar' => 'test',
            'team_id' => $team->id
        ];

        $this
            ->post(
                route('api.profiles.storeOrUpdate'),
                $data + [
                    'api_token' => $token->token
                ],
                [
                    'accept' => '/json'
                ]
            )
            ->assertResponseOk()
            ->seeInDatabase(
                'profiles',
                $data
            );

        $data['username'] = 'new';
        $this
            ->post(
                route('api.profiles.storeOrUpdate'),
                $data + [
                    'api_token' => $token->token,
                ]
            )
            ->assertResponseOk()
            ->seeInDatabase(
                'profiles',
                $data
            );
    }
}
