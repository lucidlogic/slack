<?php

namespace App\tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as LaravelTestCase;
use Laravel\Spark\Token;
use MailThief\Testing\InteractsWithMail;
use Mockery;

class TestCase extends LaravelTestCase
{
    use DatabaseTransactions;
    use InteractsWithMail;

    /**
     * @var
     */
    protected $subjectUnderTest;

    public function tearDown()
    {
        parent::tearDown();

        Mockery::close();
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';
        $app->make('Illuminate\Contracts\Console\Kernel')
            ->bootstrap();
        $this->baseUrl = config('app.url');

        return $app;
    }

    protected function refreshApplication()
    {
        $this->app = $this->createApplication();
    }

    /**
     * @return Token
     */
    public function testToken()
    {
        if ($token = Token::whereToken('test')->first()) {
            return $token;
        }

        $user = factory(User::class)->create();

        return Token::create([
            'id' => 'test',
            'user_id' => $user->id,
            'name' => 'test',
            'token' => 'test',
            'metadata' => 'test',

        ]);
    }
}
