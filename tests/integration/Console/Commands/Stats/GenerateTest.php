<?php

use App\Team;
use App\tests\TestCase;

class GenerateTest extends TestCase
{
    public function testHandle()
    {
        $team = factory(Team::class)->create();

        $data = [
            'channel' => 'acceptance-test',
            'user' => 'acceptance-test',
            'text' => 'acceptance-test',
            'team_id' => $team->id,
            'ts' => 'test',
        ];

        $this
            ->post(
                route('api.messages.store'),
                $data + [
                    'api_token' => $token->token
                ]
            )
            ->assertResponseOk()
            ->seeInDatabase(
                'messages',
                $data
            );
    }
}
