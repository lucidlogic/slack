#!/usr/bin/env bash
#
# build the webapp
#

__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# required libs
source "${__DIR__}/.bash/functions.shlib"

set -E
trap 'throw_exception' ERR

#######################################
# Display Usage Information
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
usage() {
cat <<EOF
Usage: ${0##*/} OPTIONS
build webappf

    -h          display this help and exit
    -c STRING   core version (e.g. 'dev-develop')
    -r          cache routes
EOF
}

#######################################
# Get DSN for MySQL
# Globals:
#   DB_HOST_WRITE
#   DB_USERNAME
#   DB_PASSWORD (optional)
#   DB_PORT (optional)
# Arguments:
#   None
# Returns:
#   None
#######################################
get_dsn() {
  local dsn

  dsn="-h${DB_HOST_WRITE} -u${DB_USERNAME}"

  if [[ ! -z "${DB_PASSWORD}" ]]; then
    dsn="${dsn} -p${DB_PASSWORD}"
  fi

  if [[ ! -z "${DB_PORT}" ]]; then
    dsn="${dsn} -P${DB_PORT}"
  fi

  echo "${dsn}"
}

#######################################
# Reset the Database (MySQL)
# Globals:
#   DB_DATABASE
# Arguments:
#   None
# Returns:
#   None
#######################################
reset_database() {

  if [[ ! -z "${SKIP_DATABASE}" ]]; then
    return 0
  fi

  consolelog "Starting task 'Reset Database'"

  local ret
  local dsn

  dsn=$(get_dsn)

  mysql -h127.0.0.1 -uroot -proot -e "DROP DATABASE IF EXISTS \`${DB_DATABASE}\`;"
  mysql -h127.0.0.1 -uroot -proot -e "CREATE DATABASE \`${DB_DATABASE}\`;"
}


#######################################
# Composer Install & Optionally Require
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
composer_install() {
  consolelog "Starting task 'Composer'"

  if ! composer install --prefer-dist --no-interaction --no-progress; then
    consolelog "Finished task 'Composer' with result: Error" "error"
    return 1
  fi

  consolelog "Finished task 'Composer' with result: Success" "success"
  return 0
}

#######################################
# Migrate & Optionally Seed Database (MySQL)
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
setup_database() {

  if [[ ! -z "${SKIP_DATABASE}" ]]; then
    return 0
  fi

 php artisan migrate -vv --force
 mysql -h127.0.0.1 -uroot -proot slack < database/dev.sql
 php artisan db:seed -vv
}

#######################################
# Check Routes
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
check_routes() {
  consolelog "Starting task 'Check Routes'"

  if ! php artisan route:list > /dev/null; then
    php artisan route:list -vv
    consolelog "Finished task 'Check Routes' with result: Error" "error"
    return 1
  else
    consolelog "Finished task 'Check Routes' with result: Success" "success"
    return 0
  fi
}

#######################################
# Cache Routes
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
cache_routes() {
  consolelog "Starting task 'Cache Routes'"

  if ! "${PHP_BIN}" artisan route:cache -vv; then
    consolelog "Finished task 'Cache Routes' with result: Error" "error"
    return 1
  else
    consolelog "Finished task 'Cache Routes' with result: Success" "success"
    return 0
  fi
}

#######################################
# Prepare Temporary folder for our background proccess rc's
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
prepare_subpids() {
  if [[ -d .pids_tmp ]]; then
    rm -rf .pids_tmp
  fi
  mkdir .pids_tmp
}

#######################################
# Parse Temporary folder with our background proccess rc's
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
parse_subpids() {
  for file in .pids_tmp/*; do

    # if folder is empty, globing will resolve to a literal "*"
    if [[ ! -f "${file}" ]]; then
      continue
    fi

    consolelog "ERROR!" "error"
    exit 1
  done
}

#
# pmain
#
consolelog "Starting Build for Horizon..."

prepare_subpids
  { reset_database || touch ".pids_tmp/reset_database"; } &
  { composer_install || touch ".pids_tmp/composer_install"; } &
  wait
parse_subpids

prepare_subpids
  { check_routes || touch ".pids_tmp/check_routes"; } &
  { setup_database || touch ".pids_tmp/setup_database"; } &
  if [[ ! -z "${WEBAPP_BUILD_CACHE_ROUTES}" ]]; then
    { cache_routes || touch ".pids_tmp/cache_routes"; } &
  fi
  wait
parse_subpids

consolelog "DONE!" "success"
exit 0
