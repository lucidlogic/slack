<a class="navbar-brand" href="@if(auth()->check())'/dashboard'@else'/'@endif">
    <img src="/img/mono-logo.png" style="height: 32px;">
</a>
