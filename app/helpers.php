<?php

if (!function_exists('friendlySentiment')) {

    /**
     * @param int $sentiment
     *
     * @return string
     */
    function friendlySentiment($sentiment)
    {
        if($sentiment >= 0.75) {

            return 'extremely positive';
        }

        if($sentiment > 0.5) {

            return 'very positive';
        }

        if($sentiment > 0.25) {

            return 'positive';
        }

        if($sentiment > -0.25) {

            return 'neutral';
        }

        if($sentiment > -0.5) {

            return 'negative';
        }

        if($sentiment > -0.75) {

            return 'very negative';
        }

        if($sentiment >= -1) {

            return 'extremely negative';
        }
    }
}

if (!function_exists('sentient_emoji')) {

    /**
     * @param $sentiment
     *
     * @return null
     */
    function sentient_emoji($sentiment)
    {
        $wordSentiment = friendlySentiment($sentiment);

        switch ($wordSentiment) {
            case 'extremely positive':
                return '😂';
                break;
            case 'very positive':
                return '😆';
                break;
            case 'positive':
                return '😃';
                break;
            case 'neutral':
                return '😐';
                break;
            case 'negative':
                return '😕';
                break;
            case 'very negative':
                return '☹️ ';
                break;
            case 'extremely negative':
                return '😢';
                break;

        }
    }
}


if (!function_exists('friendlyEntity')) {

    /**
     * @param $entity
     * @param bool $json
     *
     * @return mixed
     */
    function friendlyEntity($entity, $json = true)
    {
        if($json){
            $entity = array_flatten(json_decode($entity));
        }

        $collection = collect($entity);

        return $collection->unique()
            ->map(function ($entity){
                if(starts_with($entity, '@')) {
                    if($profile = \App\Profile::whereChatId(ltrim($entity, '@'))->first()) {
                        return $profile->username;
                    }
                    return null;
                }
                return trim($entity);
            })
            ->filter(function ($entity){
                return $entity ?: false;
            })
            ->first();

    }
}

if (!function_exists('my_team')) {

    /**
     * @return mixed
     */
    function my_team()
    {
        return auth()->user()
            ? auth()->user()->current_team
            : null;
    }
}

if (!function_exists('slack_button')) {

    /**
     * @return mixed
     */
    function slack_button($image = null)
    {
        if(auth()->check()){
            return null;
        }
        $image = $image?: 'add_to_slack';

        $html = "<a href='" . route('slack.redirect') . "' id='socialLogin' title='socialLogin' style='border-bottom:none' > <img alt='Add to Slack'  height='40' width='139' class='box-shadow' src='https://platform.slack-edge.com/img/{$image}.png' srcset='https://platform.slack-edge.com/img/{$image}.png 1x, https://platform.slack-edge.com/img/{$image}@2x.png 2x'  /></a>";

        return new \Illuminate\Support\HtmlString($html);
    }
}

if (!function_exists('large_emoji')) {

    /**
     * @param $sentiment
     *
     * @return \Illuminate\Support\HtmlString
     */
    function large_emoji($sentiment)
    {
        $html = "<div style='text-align: center;'><span class='large_emoji'>" . sentient_emoji($sentiment) . "</span></div>";

        return new \Illuminate\Support\HtmlString($html);
    }
}

if (!function_exists('show_intro')) {

    /**
     * @return mixed
     */
    function show_intro()
    {
        return false;
        if(auth()->guest()){

            return false;
        }

        $user = auth()->user();

        if($user->show_intro){
            $user->show_intro = false;
            $user->save();
            return true;
        }
        return false;
    }
}

if (!function_exists('is_home')) {

    /**
     * @return mixed
     */
    function is_home()
    {
       return request()->route()->getName() == 'home';
    }
}

if (!function_exists('team_url')) {

    /**
     * @return mixed
     */
    function team_url()
    {
        if($team = my_team()) {
            return "https://$team->slug.slack.com";
        }

        return null;

    }
}

