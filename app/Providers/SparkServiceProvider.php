<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Mood Climate',
        'product' => 'Mood Climate',
        'street' => '',
        'location' => '',
        'phone' => '',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = 'garrethedwards@gmail.com';

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'garrethedwards@gmail.com'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views/vendor/spark', 'spark');

        Spark::usePayPal()->noCardUpFront()->trialDays(14);

        Spark::Plan('Mood Climate', 'mood-climate')
            ->price(19)
            ->features([
                'Sentiment Analysis'
            ]);
//        Spark::noAdditionalTeams();
    }
}
