<?php

namespace App\Providers;

use App\Profile;
use App\Team;
use DB;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Log;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        $router->bind('profile', $this->bindModel(Profile::class, 'username', my_team()));

        $this->mapApiRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'middleware' => ['web', 'hasTeam'],
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    protected function mapApiRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'middleware' => 'api',
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    /**
     * @param string $modelClass
     * @param string $key
     * @param Team|null $teamId
     * @param bool $throw404
     *
     * @return callable
     */
    public function bindModel($modelClass, $key, Team $team = null, $throw404 = true)
    {
        return function ($value) use ($modelClass, $key, $team, $throw404) {
            $response = app($modelClass)
                ->where($key, $value);

            if ($team) {
                $response->whereTeamId($team->id);
            }

            $model = $response->first();

            if ($throw404 && !$model) {
                Log::debug('Model route not found', compact('modelClass', 'key', 'value'));
                abort(404);
            }

            return $model;
        };
    }
}
