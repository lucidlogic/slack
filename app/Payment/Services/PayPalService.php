<?php

namespace App\Payment\Services;

use Laravel\Spark\Contracts\Interactions\Subscribe;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PayPalService
{

    /**
     * @var \PayPal\Rest\ApiContext
     */
    protected $_api_context;

    /**
     * @var
     */
    protected $payer;

    /**
     * @var Subscription
     */
    protected $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    protected function initialize()
    {
        // setup PayPal api context
        $this->_api_context = new ApiContext(
            new OAuthTokenCredential(
                config('services.paypal.client_id'),
                config('services.paypal.secret'
                )
            )
        );
        $this->_api_context->setConfig(config('services.paypal.settings'));
        $this->payer->setPaymentMethod('paypal');
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function complete(array $data)
    {
        $this->initialize();
        $payment = Payment::get(array_get($data, 'paymentId'), $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(array_get($data, 'PayerID'));
        if ($result = $payment->execute($execution, $this->_api_context)) {
            $plan = Spark::plans()->first();

            Spark::interact(Subscribe::class, [
                auth()->user(),
                $plan,
                false,
                request()->all() +
                [
                    'stripe_id' => array_get($data, 'paymentId')
                ]
            ]);

            return true;
        }

        return false;
    }
}
