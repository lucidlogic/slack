<?php

namespace App\Repositories;

use App\Team;
use App\User;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Laravel\Spark\Contracts\Interactions\Settings\Teams\AddTeamMember as AddTeamMemberContract;
use Laravel\Spark\Events\Teams\TeamCreated;
use Laravel\Spark\Repositories\TeamRepository as BaseRepository;
use Laravel\Spark\Spark;

class TeamRepository extends BaseRepository
{
    /**
     * @param string $slackId
     *
     * @return Team
     */
    public function getTeamSlackId($slackId)
    {
        return Team::whereSlackId($slackId)->first();
    }

    /**
     * @param User $user
     * @param SocialiteUser $socialUser
     *
     * @return Team
     */
    public function createSocial(User $user, SocialiteUser $socialUser)
    {
        $data = [
            'name' => $socialUser->user['team']['name'],
            'slug' => $socialUser->user['team']['domain'],
        ];

        event(new TeamCreated($team = Spark::interact(
            TeamRepository::class . '@create', [$user, $data]
        )));

        Spark::interact(AddTeamMemberContract::class, [
            $team,
            $user,
            'owner',
        ]);
        $team->slug = array_get($socialUser->user['team'], 'domain');
        $team->photo_url =  array_get($socialUser->user['team'], 'image_68');
        $team->slack_id = $socialUser->organization_id;
        $team->save();

        return $team;
    }
}
