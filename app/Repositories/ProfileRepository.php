<?php

namespace App\Repositories;

use App\Profile;
use App\Team;
use Illuminate\Database\Eloquent\Collection;

class ProfileRepository
{
    /**
     * @param array $data
     *
     * @return Profile
     */
    public function create(array $data)
    {
        return Profile::create([
            'chat_id' => array_get($data, 'chat_id'),
            'username' => array_get($data, 'username'),
            'first_name' => array_get($data, 'first_name'),
            'last_name' => array_get($data, 'last_name'),
            'avatar' => array_get($data, 'avatar'),
            'email' => array_get($data, 'email'),
            'is_admin' => array_get($data, 'is_admin'),
            'is_owner' => array_get($data, 'is_owner'),
            'is_restricted' => array_get($data, 'is_restricted'),
            'is_bot' => array_get($data, 'is_bot'),
            'team_id' => array_get($data, 'team_id'),
        ]);
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return Profile
     */
    public function update($id, array $data)
    {
        $profile = Profile::findOrFail($id);

        foreach ($data as $key => $value) {
            if (isset($data[$key]) && in_array($key, array_keys($profile->getAttributes()))) {
                $profile->$key = $data[$key];
            }
        }

        $profile->save();

        return $profile;
    }

    /**
     * @param int $id
     *
     * @return Profile
     */
    public function find($id)
    {
        return Profile::find($id);
    }

    /**
     * @param int $chatId
     *
     * @return Profile
     */
    public function findByChatId($chatId)
    {
        return Profile::whereChatId($chatId)->first();
    }

    /**
     * @param Team $team
     *
     * @return Collection
     */
    public function all(Team $team)
    {
        return Profile::whereTeamId($team->id)->get();
    }
}
