<?php

namespace App\Repositories;

use App\Profile;
use App\Stat;
use App\Team;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Collection;

class StatRepository
{
    /**
     * @var MessageRepository
     */
    protected $messageRepository;

    /**
     * @var ProfileRepository
     */
    protected $profileRepository;

    /**
     * @var SentimentRepository
     */
    protected $sentimentRepository;

    /**
     * @param MessageRepository $messageRepository
     * @param SentimentRepository $sentimentRepository
     * @param ProfileRepository $profileRepository
     */
    public function __construct(
        MessageRepository $messageRepository,
        SentimentRepository $sentimentRepository,
        ProfileRepository $profileRepository
    ) {
        $this->sentimentRepository = $sentimentRepository;
        $this->messageRepository = $messageRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param array $data
     *
     * @return Stat
     */
    public function create(array $data)
    {
        return Stat::firstOrCreate([
            'date' => array_get($data, 'date'),
            'entity' => array_get($data, 'entity'),
            'messages' => array_get($data, 'messages'),
            'profile_id' => array_get($data, 'profile_id'),
            'sentiment' => array_get($data, 'sentiment'),
            'team_id' => array_get($data, 'team_id'),
            'type' => array_get($data, 'type'),

        ]);
    }

    /**
     * @param Team $team
     * @param Carbon $date
     * @param Profile|null $profile
     * @param null $entity
     * @param string $type
     */
    public function createStats(
        Team $team,
        Carbon $date,
        $type,
        Profile $profile = null,
        $entity = null
    ) {
        $this->create([
            'date' => $date->toDateString(),
            'type' => $type,
            'sentiment' => $this->sentimentRepository->getAverageSentiment($date, $team->id, $profile, $entity) ?: 0,
            'messages' => $this->messageRepository->getTotal($date, $team, $profile, $entity) ?: 0,
            'team_id' => $team->id,
            'profile_id' => $profile ? $profile->id : null,
            'entity' => $entity ?: null,
        ]);
    }

    /**
     * @param Team $team
     * @param string|null $date
     */
    public function generateStats(Team $team, $date = null)
    {
        if ( ! $date) {
            $date = Carbon::now();
        }

        //General report (all profiles & entities)
        $this->createStats($team, $date, 'all');

        //profiles
        $this
            ->profileRepository
            ->all($team)
            ->each(function ($profile) use ($team, $date) {
                // specific profile, all entities
                // Dave was positive today
                $this->createStats($team, $date, 'profile', $profile);
                // Specific profile, Specific entity
                $this
                    ->sentimentRepository
                    ->getEntities($team, $date, $profile)
                    ->each(function ($sentiment) use ($team, $date, $profile) {
                        // save avg sentiment about this entity for this profile
                        // dave loves beer today
                        $this->createStats(
                            $team,
                            $date,
                            'profile',
                            $profile,
                            $sentiment->entities
                        );
                    });
            });
    }

    /**
     * @param Team $team
     * @param $days
     * @param Profile|null $profile
     * @param $entity
     *
     * @return mixed
     */
    public function getGeneralStats(
        Team $team,
        $days,
        Profile $profile = null,
        string $entity = null
    ) {
        switch ($days) {
            case 365:
                $collection = $this->getMonthlyStats($team, $profile, $entity);
                break;
            case 30:
            default:
                $collection = $this->getDailyStats($team, $days, $profile, $entity);
                break;
        }

        return $collection;
    }

    /**
     * @param Team $team
     * @param $days
     * @param Profile|null $profile
     * @param string $entity
     *
     * @return mixed
     */
    public function getDailyStats(
        Team $team,
        $days,
        Profile $profile = null,
        string $entity = null
    ) {
        $query = Stat::whereTeamId($team->id)
                     ->whereType($profile || $entity ? 'profile' : 'all')
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay($days)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     );

        if ($profile) {
            $query->whereProfileId($profile->id);
        }

        if($entity) {
            $query->where('entity', 'LIKE', $entity);
        }

        $query->orderBy('id', 'DESC');

        return $query->get();
    }

    /**
     * @param Team $team
     * @param Profile|null $profile
     * @param null $entity
     *
     * @return mixed
     */
    public function getMonthlyStats(
        Team $team,
        Profile $profile = null,
        $entity = null
    ) {
        $query = Stat::select(DB::raw('avg(sentiment) as avg_sentiment, MONTH(date) as month'))
                     ->whereType($profile || $entity ? 'profile' : 'all')
                     ->whereTeamId($team->id)
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay(365)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     );
        if ($profile) {
            $query->whereProfileId($profile->id);
        }

        if($entity) {
            $query->where('entity', 'LIKE', $entity);
        }

        $query->orderBy('month', 'DESC')
              ->groupBy('month');

        return $query->get();
    }

    /**
     * @param Team $team
     * @param $days
     * @param null $entity
     *
     * @return Collection
     */
    public function getProfiles(
        Team $team,
        $days,
        $entity = null
    ) {
        $query = Stat::select(DB::raw('avg(sentiment) as avg_sentiment, profile_id, count(*) as message_count'))
                     ->whereTeamId($team->id)
                     ->whereType('profile')
                     ->whereNotNull('profile_id')
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay($days)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     );

        if ($entity) {
            $query->where('entity', 'LIKE', $entity);
        }

        return $query
            ->groupBy('profile_id')
            ->get();
    }

    /**
     * @param Team $team
     * @param int $days
     * @param Profile $profile
     *
     * @return Collection
     */
    public function getEntities(
        Team $team,
        int $days,
        Profile $profile = null
    ) {
        $query = Stat::select(DB::raw('avg(sentiment) as avg_sentiment, entity, count(*) as message_count'))
                     ->whereTeamId($team->id)
                     ->whereType('profile')
                     ->whereNotNull('entity')
                     ->where('entity', '<>', '[]')
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay($days)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     );
        if ($profile) {
            $query->whereProfileId($profile->id);
        }

         return $query
            ->groupBy('entity')
            ->get();
    }

    /**
     * @param Team $team
     * @param $days
     * @param Profile $profile
     *
     * @return Collection
     */
    public function getProfileStats(
        Team $team,
        $days,
        Profile $profile

    ) {
        $query = Stat::select(DB::raw('avg(sentiment) as sentiment, date'))
                     ->whereTeamId($team->id)
                     ->whereType('profile')
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay($days)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     )
                     ->whereProfileId($profile->id)
                     ->groupBy('date');

        return $query->get();
    }

    /**
     * @param Team $team
     * @param $days
     * @param $entity
     *
     * @return Collection
     */
    public function getEntityStats(
        Team $team,
        $days,
        $entity
    ) {
        $query = Stat::select(DB::raw('avg(sentiment) as sentiment, date'))
                     ->whereTeamId($team->id)
                     ->whereType('profile')
                     ->whereBetween(
                         'date',
                         [
                             Carbon::now()->subDay($days)->toDateString(),
                             Carbon::now()->toDateString(),
                         ]
                     )
                     ->where('entity', 'LIKE', $entity)
                     ->groupBy('date');

        return $query->get();
    }
}
