<?php

namespace App\Repositories;

use App\Message;
use App\Profile;
use App\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MessageRepository
{
    /**
     * @param array $data
     *
     * @return Message Message
     */
    public function create(array $data)
    {
        return Message::create([
            'channel' => array_get($data, 'channel'),
            'user' => array_get($data, 'user'),
            'text' => array_get($data, 'text'),
            'team_id' => array_get($data, 'team_id'),
            'ts' => array_get($data, 'ts'),
            'event' => array_get($data, 'event'),
        ]);
    }

    /**
     * @param Carbon $date
     * @param Team $team
     * @param null $profile
     * @param null $entity
     *
     * @return float
     */
    public function getTotal(
        Carbon $date,
        Team $team,
        $profile = null,
        $entity = null
    ) {
        $builder = Message::whereTeamId($team->id)
                          ->onThisDay($date);
        if ($profile) {
            $builder->whereUser($profile->chat_id);
        }

        if ($entity) {
            $builder->whereHas('sentiment', function ($sentiment) use ($entity) {
                $sentiment->where('entities', 'like', $entity);
            });
        }

        return $builder->count();
    }

    /**
     * @param Team $team
     * @param Carbon $date
     *
     * @return Collection
     */
    public function getProfiles(
        Team $team,
        Carbon $date
    ) {
        return Profile::whereHas('messages', function ($messages) use ($date) {
                   $messages->onThisDay($date);
               })
               ->team($team->id)
               ->get();
    }
}
