<?php

namespace App\Repositories;

use App\User;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Laravel\Spark\Repositories\UserRepository as BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @param string $token
     *
     * @return User
     */
    public function findByToken($token)
    {
        return User::where('social_token', $token)->first();
    }

    /**
     * @param SocialiteUser $socialUser
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function createSocial(SocialiteUser $socialUser)
    {
        if ($user = User::whereEmail($socialUser->email)->first()) {
            $user->social_token = $socialUser->token;
            $user->save();

            return $user;
        }

        $user = $this->create([
            'name' => $socialUser->name,
            'email' => $socialUser->email,
            'password' => $socialUser->token,
        ]);
        $user->photo_url = $socialUser->avatar;
        $user->social_token = $socialUser->token;
        $user->save();

        return $user;
    }

}
