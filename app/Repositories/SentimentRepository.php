<?php

namespace App\Repositories;

use App\Events\SentimentSaved;
use App\Profile;
use App\Sentiment;
use App\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class SentimentRepository
{
    /**
     * @param array $data
     *
     * @return Sentiment
     */
    public function create(array $data)
    {
        $sentiment =  Sentiment::create([
            'message_id' => array_get($data, 'message_id'),
            'sentiment' => array_get($data, 'sentiment'),
            'entities' => array_get($data, 'entities'),
            'team_id' => array_get($data, 'team_id'),
        ]);

        event(new SentimentSaved($sentiment));
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {

        $sentiment = Sentiment::findOrFail($id);

        foreach ($data as $key => $value) {
            if (isset($data[$key]) && in_array($key, array_keys($sentiment->getAttributes()))) {
                $sentiment->$key = $data[$key];
            }
        }

        $sentiment->save();

        event(new SentimentSaved($sentiment));

        return $sentiment;
    }

    /**
     * @param int $id
     *
     * @return Sentiment
     */
    public function find($id)
    {
        return Sentiment::find($id);
    }

    /**
     * @param int $messageId
     *
     * @return Sentiment
     */
    public function findByMessageId($messageId)
    {
        return Sentiment::whereMessageId($messageId)->first();
    }

    /**
     * @param Carbon $date
     * @param int $teamId
     * @param null $profile
     * @param null $entity
     *
     * @return float
     */
    public function getAverageSentiment(
        Carbon $date,
        $teamId,
        $profile = null,
        $entity = null
    ) {
        $builder = Sentiment::team($teamId)
                            ->onThisDay($date);
        if ($profile) {
            $builder->profile($profile);
        }

        if ($entity) {
            $builder->where('entities', 'like', $entity);
        }

        return $builder->avg('sentiment');
    }


    /**
     * @param Team $team
     * @param Carbon $date
     * @param Profile|null $profile
     *
     * @return Collection
     */
    public function getEntities(
        Team $team,
        Carbon $date,
        Profile $profile = null
    ) {
        $query = Sentiment::team($team->id)
                          ->onThisDay($date)
                          ->select('entities');

        // Only return entities that Dave has been speaking about
        if ($profile) {
            $query->whereHas('message', function ($message) use ($profile) {
                $message->whereUser($profile->chat_id);
            });
        }

        return $query->distinct('entities')
                     ->get();

    }

    /**
     * @param string $entity
     * @param int $teamId
     *
     * @return bool
     */
    public function isBeingBullied(
        $entity,
        int $teamId
    ) {
        return $this->getAverageSentiment(
            Carbon::now(),
            $teamId,
            null,
            $entity
        ) < config('bully.sentiment');
    }
}
