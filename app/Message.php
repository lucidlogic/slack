<?php

namespace App;

use App\Traits\DayScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Message extends Model
{
    use DayScope;

    public static $rules = [
        'channel' => 'required',
        'user' => 'required',
        'text' => 'required',
        'team_id' => 'required|exists:teams,id',
    ];

    protected $fillable = [
        'channel',
        'user',
        'text',
        'team_id',
        'ts',
        'event',
    ];

    /**
     * @return HasOne
     */
    public function sentiment()
    {
        return $this->hasOne(Sentiment::class, 'message_id');
    }

    /**
     * @return HasOne
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    /**
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'chat_id');
    }
}
