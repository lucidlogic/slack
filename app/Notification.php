<?php

namespace App;

use App\Traits\MinutesScope;
use Laravel\Spark\Notification as SparkNotification;

class Notification extends SparkNotification
{
    use MinutesScope;

}
