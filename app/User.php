<?php

namespace App;

use App\Traits\CanJoinTeams;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Spark\Announcement;
use Laravel\Spark\Notification;
use Laravel\Spark\Subscription;
use Laravel\Spark\Token;
use Laravel\Spark\User as SparkUser;

class User extends SparkUser
{
    use CanJoinTeams;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    /**
     * @return HasMany
     */
    public function announcements()
    {
        return $this->hasMany(Announcement::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function tokens()
    {
        return $this->hasMany(Token::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class, 'user_id');
    }
}
