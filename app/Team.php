<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Laravel\Spark\Team as SparkTeam;

class Team extends SparkTeam
{
    /**
     * @param Builder $query
     * @param int $id
     *
     * @return Builder
     */
    public function scopeSlackId(Builder $query, $id)
    {
        return $query->where('slack_id', $id);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSubscribed(Builder $query)
    {
        return $query->whereNotNull('slack_id');
    }
}
