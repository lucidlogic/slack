<?php

namespace App\Console\Commands\Stats;

use App\Repositories\EmailService;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TrialEnding extends Command
{
    /**
     * @var EmailService
     */
    protected $emailService;

    /**
     * @var string
     */
    protected $signature = 'subscriptions:trial-ending {--days=0}';

    /**
     * @var string
     */
    protected $description = 'Send Emails for trials ending in x days';

    /**
     * @param EmailService $emailService
     */
    public function __construct(
        EmailService $emailService
    ) {
        parent::__construct();
        $this->emailService = $emailService;
    }

    public function handle()
    {
        $days = (int) $this->option('days') ?: 0;
        $fromDate = Carbon::now()->subDays($days);
        $users = User::whereBetween(
            'trial_end_at',
            [
                $fromDate->startOfDay(),
                $fromDate->endOfDay(),
            ]
        );

        $this->info("Sending Emails For {$users->count()} subscribed users");

        $users->each(function ($user) use ($days){

            if($days){
                $this->emailService->trialEnded($user);
            } else {
                $this->emailService->trialEnding($user);
            }

            $this->info("Sent email to {$user->email}");
        });

        $this->info("Done :)");
    }
}
