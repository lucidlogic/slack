<?php

namespace App\Console\Commands\Stats;

use App\Repositories\StatRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Team;

class Generate extends Command
{
    /**
     * @var StatRepository
     */
    protected $statRepository;

    /**
     * @var string
     */
    protected $signature = 'stats:generate {--days=0}';

    /**
     * @var string
     */
    protected $description = 'Generate stats per Team';

    /**
     * @param StatRepository $statRepository
     */
    public function __construct(
        StatRepository $statRepository
    ) {
        parent::__construct();
        $this->statRepository = $statRepository;
    }

    public function handle()
    {
        $teams = Team::whereNotNull('slack_id');

        $this->info("Generating Stats for {$teams->count()} subscribed teams");

        $teams->each(function($team){
            $days = (int) $this->option('days') ?: 0;
            //stats for the past x days
            if($days){
                $i = 0;
                while ($i <= $days) {
                    $date = Carbon::now()->subDays($i);
                    $this->info("Generating Stats for {$team->name} on {$date->toDateString()}");
                    $this->statRepository->generateStats($team, $date);
                    $i++;
                }
            } else {
                $this->info("Generating Stats for {$team->name} for today");
                $this->statRepository->generateStats($team);
            }

            $this->info("Generated All Stats for {$team->name}");
        });

        $this->info("Done :)");
    }
}
