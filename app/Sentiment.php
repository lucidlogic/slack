<?php

namespace App;

use App\Traits\DayScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Sentiment extends Model
{
    use DayScope;

    /**
     * @var array
     */
    public static $rules = [
        'message_id' => 'required|exists:messages,id',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'message_id',
        'sentiment',
        'entities',
    ];

    /**
     * @return BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(Message::class, 'message_id');
    }

    /**
     * @param Builder $query
     * @param int $id
     *
     * @return Builder
     */
    public function scopeTeam(Builder $query, $id)
    {
        return $query->whereHas('message', function ($message) use ($id) {
            $message->whereTeamId($id);
        });
    }

    /**
     * @param Builder $query
     * @param Profile $profile
     *
     * @return Builder
     */
    public function scopeProfile(Builder $query, Profile $profile)
    {
        return $query->whereHas('message', function ($message) use ($profile) {
            $message->whereUser($profile->chat_id);
        });
    }
}
