<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Stat extends Model
{
    public static $rules = [
        'date' => 'required',
        'type' => 'required',
        'sentiment' => 'required',
        'messages' => 'required|numeric',

    ];

    protected $fillable = [
        'date',
        'type',
        'sentiment',
        'messages',
        'entity',
        'profile_id',
        'team_id'
    ];

    /**
     * @return BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id');
    }
}
