<?php
use App\Http\Requests\Settings\PaymentMethod\UpdatePayPalPaymentMethodRequest;
use App\Http\Requests\Settings\Subscription\CreatePayPalSubscriptionRequest;
use App\Interactions\Settings\PaymentMethod\RedeemPayPalCoupon;
use App\Interactions\Settings\PaymentMethod\UpdatePayPalPaymentMethod;
use App\Interactions\SubscribeTeamUsingPayPal;
use App\Repositories\PayPalCouponRepository;
use Laravel\Spark\Contracts\Http\Requests\Settings\PaymentMethod\UpdatePaymentMethodRequest;
use Laravel\Spark\Contracts\Http\Requests\Settings\Subscription\CreateSubscriptionRequest;
use Laravel\Spark\Contracts\Interactions\Settings\PaymentMethod\RedeemCoupon;
use Laravel\Spark\Contracts\Interactions\Settings\PaymentMethod\UpdatePaymentMethod;
use Laravel\Spark\Contracts\Interactions\Subscribe;
use Laravel\Spark\Contracts\Repositories\CouponRepository;

// Interactions
App::bind(RedeemCoupon::class, RedeemPayPalCoupon::class);
App::bind(Subscribe::class, SubscribeTeamUsingPayPal::class);
App::bind(UpdatePaymentMethod::class, UpdatePayPalPaymentMethod::class);

// Repositories
App::bind(CouponRepository::class, PayPalCouponRepository::class);

// Requests
App::bind(CreateSubscriptionRequest::class, CreatePayPalSubscriptionRequest::class);
App::bind(UpdatePaymentMethodRequest::class, UpdatePayPalPaymentMethodRequest::class);
