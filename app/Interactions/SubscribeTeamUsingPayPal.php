<?php

namespace App\Interactions;

use Laravel\Spark\Interactions\SubscribeTeam;

class SubscribeTeamUsingPayPal extends SubscribeTeam
{
    /**
     * The token field to be used during subscription.
     *
     * @var string
     */
    protected $token = 'paypal_token';
}
