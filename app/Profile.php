<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Profile extends Model
{
    public static $rules = [
        'chat_id' => 'required',
        'email' => 'required|email',
        'is_admin' => 'bool',
        'team_id' => 'required|exists:teams,id',
        'username' => 'required',
    ];

    protected $fillable = [
        'avatar',
        'chat_id',
        'email',
        'first_name',
        'last_name',
        'is_admin',
        'is_bot',
        'is_owner',
        'is_restricted',
        'last_name',
        'team_id',
        'username',

    ];

    /**
     * @return HasOne
     */
    public function team()
    {
        return $this->hasOne(Sentiment::class, 'message_id');
    }

    /**
     * @return HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'user', 'chat_id');
    }

    /**
     * @param Builder $query
     * @param int $id
     *
     * @return Builder
     */
    public function scopeTeam(Builder $query, $id)
    {
        return $query->where('team_id', $id);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name && $this->last_name
            ? $this->first_name . ' ' . $this->last_name . ' (' . $this->username . ')'
            : $this->username;
    }
}
