<?php

namespace App\Repositories;

use App\Message;
use App\Profile;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Mail;

class EmailService
{
    /**
     * @param User $user
     */
    public function trialEnded(User $user)
    {
        Mail::queue(
            'emails.subscriptions.trialEnded',
            [
                'name' => $user->name
            ]
        );
    }

    /**
     * @param User $user
     */
    public function trialEnding(User $user)
    {
        Mail::queue(
            'emails.subscriptions.trialEnding',
            [
                'name' => $user->name,
                'ends' => $user->trial_ends_at->diffForHumans()
            ]
        );
    }

    /**
     * @param User $user
     */
    public function subscriptionStarted(User $user)
    {
        Mail::queue(
            'emails.subscriptions.subscriptionStarted',
            [
                'name' => $user->name
            ]
        );
    }

    /**
     * @param User $user
     */
    public function subscriptionCancelled(User $user)
    {
        Mail::queue(
            'emails.subscriptions.subscriptionCancelled',
            [
                'name' => $user->name
            ]
        );
    }

    /**
     * @param User $user
     */
    public function subscriptionFailed(User $user)
    {
        Mail::queue(
            'emails.subscriptions.subscriptionFailed',
            [
                'name' => $user->name,
                'card_brand' => $user->card_brand,
                'card_last_four' => $user->card_last_four,
            ]
        );
    }
}
