<?php

namespace App\Events;

use App\Sentiment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class SentimentSaved
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var Sentiment
     */
    public $sentiment;

    /**
     * SentimentSaved constructor.
     *
     * @param Sentiment $sentiment
     */
    public function __construct(Sentiment $sentiment)
    {
        $this->sentiment = $sentiment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
