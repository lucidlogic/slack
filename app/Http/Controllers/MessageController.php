<?php

namespace App\Http\Controllers;

use App\Repositories\MessageRepository;
use App\Team;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    /**
     * @var MessageRepository
     */
    protected $messageRepository;

    /**
     * @param MessageRepository $messageRepository
     */
    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request['team_id'] = Team::slackId($request['team'])->first()->id;

        $this->validate($request, [
            'channel' => 'required',
            'user' => 'required',
            'text' => 'required',
            'team_id' => 'required|exists:teams,id',
        ]);

        return response()
            ->json([
                'message' => $this
                    ->messageRepository
                    ->create($request->input()),
            ]);
    }
}
