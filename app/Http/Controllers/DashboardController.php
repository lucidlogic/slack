<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Repositories\StatRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @var StatRepository
     */
    protected $statRepository;

    /**
     * @param StatRepository $statRepository
     */
    public function __construct(StatRepository $statRepository)
    {
        $this->statRepository = $statRepository;
       // $this->middleware('subscribed');
    }

    /**
     * Main dashboard view
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function general(Request $request)
    {
        $lineData = $this->getLineData($request);

        $profileStats = $this->getProfileData($request);

        $entityStats = $this->getEntityData($request);

        if ( ! count($lineData['points'])) {

            return view('dashboard.new');
        }

        return view('dashboard.stats', compact('lineData', 'profileStats', 'entityStats'));
    }

    /**
     * Profile Dashboard, stats on a particular profile over time
     *
     * @param Request $request
     * @param Profile $profile
     *
     * @return \Illuminate\View\View
     */
    public function profile(
        Request $request,
        Profile $profile
    ) {
        $lineData = $this->getLineData($request, $profile);

        $entityStats = $this->getEntityData($request, $profile);

        return view('dashboard.stats', compact('lineData', 'profileStats', 'entityStats', 'profile'));
    }

    /**
     * Entity/topic Dashboard, stats on a particular topic over time
     *
     * @param Request $request
     * @param string $entity
     *
     * @return \Illuminate\View\View
     */
    public function entity(
        Request $request,
        string $entity
    ) {
        $entity = urldecode($entity);

        $lineData = $this->getLineData($request, null, $entity);

        $sentiment = collect($lineData['points'])->avg();

        $profileStats = $this->getProfileData($request, $entity);

        return view('dashboard.stats', compact('lineData', 'profileStats', 'entityStats', 'entity', 'sentiment'));
    }

    /**
     * @param Request $request
     * @param Profile|null $profile
     * @param string|null $entity
     *
     * @return array
     */
    protected function getLineData(
        Request $request,
        Profile $profile = null,
        string $entity = null
    ) {
        list($days, $title) = $this->getInterval($request->input('interval'), $profile, $entity);

        $stats = $this
            ->statRepository
            ->getGeneralStats(
                my_team(),
                $days,
                $profile,
                $entity
            );

        $labels = $this->getLabels($stats, $days);

        $points = $stats
            ->map(function ($stat) use ($days) {

                if ($days == 365) {
                    return round($stat->avg_sentiment, 3);
                }

                return round($stat->sentiment, 3);

            })
            ->all();

        return [
            'labels' => $labels,
            'points' => $points,
            'title' => $title,
        ];
    }

    /**
     * @param $interval
     * @param Profile|null $profile
     *
     * @return array
     */
    protected function getInterval($interval, Profile $profile = null, $entity = null)
    {
        $person = 'Your team';
        $about = '';

        if ($profile) {
            $person = $profile->first_name ?: $profile->username;
        }

        if ($entity) {
            $about = " about  '" . friendlyEntity($entity) . "'";
        }

        $days = 7;

        if ($interval == 'week') {
            $days = 7;
        }

        if ($interval == 'month') {
            $days = 30;
        }

        if ($interval == 'year') {
            $days = 365;
        }
        return [
            $days,
            "$person's sentiment $about",
        ];
    }

    /**
     * @param Request $request
     * @param null $entity
     *
     * @return mixed
     */
    protected function getProfileData(
        Request $request,
        $entity = null
    ) {
        list($days) = $this->getInterval($request->input('interval'));

        return $this
            ->statRepository
            ->getProfiles(
                my_team(),
                $days,
                $entity
            );
    }

    /**
     * @param Request $request
     * @param Profile $profile
     *
     * @return mixed
     */
    protected function getEntityData(
        Request $request,
        Profile $profile = null
    ) {
        list($days) = $this->getInterval($request->input('interval'));

        return $this
            ->statRepository
            ->getEntities(
                my_team(),
                $days,
                $profile
            );
    }

    /**
     * @param $stats
     * @param int $days
     *
     * @return mixed
     */
    public function getLabels($stats, int $days)
    {
        switch ($days) {
            case 365:
                $labels = $stats
                    ->map(function ($stat) {
                        return date('M', mktime(0, 0, 0, $stat->month, 10));
                    })
                    ->all();
                break;
            case 30:
                $labels = $stats
                    ->map(function ($stat) {
                        return date('d M', strtotime($stat->date));
                    })
                    ->every(2)
                    ->all();
                break;
            default:
                $labels = $stats
                    ->map(function ($stat) {
                        return date('D', strtotime($stat->date));
                    })
                    ->all();
                break;
        }

        return $labels;

    }
}
