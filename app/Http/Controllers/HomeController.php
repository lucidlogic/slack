<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show()
    {
        return redirect()->route('user.dashboard.general');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function privacy()
    {
        return view('privacy');
    }
}
