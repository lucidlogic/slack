<?php

namespace App\Http\Controllers;

use App\Repositories\SentimentRepository;
use App\Sentiment;
use App\Team;
use Illuminate\Http\Request;

class SentimentController extends Controller
{

    /**
     * @var SentimentRepository
     */
    protected $sentimentRepository;

    /**
     * @param SentimentRepository $sentimentRepository
     */
    public function __construct(SentimentRepository $sentimentRepository)
    {
        $this->sentimentRepository = $sentimentRepository;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrUpdate(Request $request)
    {
        $this->validate($request, [
            'message_id' => 'required|exists:messages,id',
        ]);
        if ($sentiment = $this->sentimentRepository->findByMessageId($request->input('message_id'))) {
            $result = $this
                ->sentimentRepository
                ->update(
                    $sentiment->id,
                    $request->input()
                );
        } else {
            $result = $this
                ->sentimentRepository
                ->create($request->input());
        }

        return response()
            ->json(['sentiment' => $result]);
    }
}
