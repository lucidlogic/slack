<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Repositories\MessageRepository;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class WelcomeController extends Controller
{
    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function show()
    {
        return view('welcome');
    }
}
