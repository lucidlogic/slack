<?php

namespace App\Http\Controllers\Settings\Subscription;

use App\Http\Controllers\Controller;
use App\Payment\Services\PayPalService;

class PayPalController extends Controller
{
    /**
     * @var PayPalService
     */
    protected $payPalService;

    /**
     * PayPalController constructor.
     *
     * @param PayPalService $payPalService
     */
    public function __construct(PayPalService $payPalService)
    {
        $this->payPalService = $payPalService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function success()
    {
        if ( ! $this->payPalService->complete(request()->input())) {
            return redirect('/settings/#subscribe')->withMessage('Looks like something went wrong, try again.');
        }

        return redirect('/settings/#subscribe')->withMessage('Greate Success!');
    }
}
