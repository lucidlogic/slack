<?php

namespace App\Http\Controllers;

use App\Repositories\ProfileRepository;
use App\Team;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    /**
     * @var ProfileRepository
     */
    protected $profileRepository;

    /**
     * @param ProfileRepository $profileRepository
     */
    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrUpdate(Request $request)
    {
        $request['team_id'] = Team::slackId($request['team'])->first()->id;

        $this->validate($request, [
            'chat_id' => 'required|alpha_dash',
            'email' => 'email',
            'is_admin' => 'bool',
            'team_id' => 'required|exists:teams,id',
            'username' => 'required',
        ]);

        if ($profile = $this
            ->profileRepository
            ->findByChatId($request->input('chat_id'))
        ) {

            $result = $this
                ->profileRepository
                ->update(
                    $profile->id,
                    $request->input()
                );
        } else {
            $result = $this
                ->profileRepository
                ->create($request->input());
        }

        return response()
            ->json(['profile' => $result]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        if ($profile = $this->profileRepository->find($id)) {
            return response()
                ->json(['profile' => $profile]);
        }

        abort(404);
    }
}
