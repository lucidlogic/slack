<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Two\InvalidStateException;
use Laravel\Spark\Events\Auth\UserRegistered;
use Laravel\Spark\Events\Teams\TeamMemberAdded;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard/';

    /**
     * @var TeamRepository
     */
    protected $teamRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     * @param TeamRepository $teamRepository
     */
    public function __construct(
        UserRepository $userRepository,
        TeamRepository $teamRepository
    ) {
        $this->middleware('guest', ['except' => 'logout']);
        $this->teamRepository = $teamRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('slack')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            if (request()->has('error') || ! $socialUser = Socialite::driver('slack')->user()) {

                return redirect()->route('home')->withMessage('There was a problem, please try again.');
            }
        } catch (InvalidStateException $e) {
            \Log::error($e, ['Slack Auth Error']);

            return redirect()->route('home')->withMessage('There was a problem, please try again.');
        }


        if ($user = $this->userRepository->findByToken($socialUser->token)) {

            return $this->socialLogin($user);
        }

        $user = $this->userRepository->createSocial($socialUser);

        event(new UserRegistered($user));

        if ( ! $team = $this->teamRepository->getTeamSlackId($socialUser->organization_id)) {
            $this->teamRepository->createSocial($user, $socialUser);

        } else {
            $user->teams()->attach($team->id, ['role' => 'member']);
            event(new TeamMemberAdded($team, $user));
        }

        return $this->socialLogin($user);
    }

    /**
     * @param Authenticatable $user
     *
     * @return mixed
     */
    protected function socialLogin(Authenticatable $user)
    {
        auth()->login($user);

        //mind the emoji :)
        return redirect()
            ->to($this->redirectTo)
            ->withMessage("Welcome {$user->first_name} 😸");
    }
}
