<?php

namespace App\Interactions\Settings\PaymentMethod;

use Laravel\Spark\Contracts\Interactions\Settings\PaymentMethod\UpdatePaymentMethod;

class UpdatePayPalPaymentMethod implements UpdatePaymentMethod
{
    /**
     * {@inheritdoc}
     */
    public function handle($billable, array $data)
    {
        if ($billable->paypal_id) {
            $billable->updateCard($data['paypal_token']);
        } else {
            $billable->createAsBraintreeCustomer($data['paypal_token']);
        }
    }
}
