<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait DayScope
{
    /**
     * @param Builder $query
     * @param Carbon $date
     *
     * @return Builder
     */
    public function scopeOnThisDay(Builder $query, Carbon $date)
    {
        $queryDate = $date->copy();

        return $query->whereBetween(
            'created_at',
            [
                $date->toDateString(),
                $queryDate->addDay(1)->toDateString(),
            ]
        );
    }
}
