<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait MinutesScope
{
    /**
     * @param Builder $query
     * @param int $minutes
     *
     * @return Builder
     */
    public function scopeMinutes(Builder $query, int $minutes)
    {
        return $query->whereBetween(
            'created_at',
            [
                Carbon::now()->subMinutes($minutes),
                Carbon::now(),
            ]
        );
    }
}
