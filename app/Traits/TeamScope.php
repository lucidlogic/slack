<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait TeamScope
{

    /**
     * @param Builder $query
     * @param int $id
     *
     * @return Builder
     */
    public function scopeTeam(Builder $query, $id)
    {
        return $query->where('team_id', $id);
    }
}
