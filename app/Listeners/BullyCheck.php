<?php

namespace App\Listeners;

use App\Events\SentimentSaved;
use App\Notification;
use App\Repositories\SentimentRepository;
use Laravel\Spark\Repositories\NotificationRepository;

class BullyCheck
{
    /**
     * @var NotificationRepository
     */
    protected $notificationRepository;

    /**
     * @var SentimentRepository
     */
    protected $sentimentRepository;

    /**
     * @param SentimentRepository $sentimentRepository
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(
        SentimentRepository $sentimentRepository,
        NotificationRepository $notificationRepository
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->sentimentRepository = $sentimentRepository;
    }

    /**
     * Handle the event.
     *
     * @param  SentimentSaved $event
     *
     * @return void
     */
    public function handle(SentimentSaved $event)
    {
        collect(json_decode($event->sentiment->entities))
            ->filter(function ($entity) {
                return starts_with($entity, '@');
            })
            ->each(function ($entity) use ($event) {

                $message = trans('bully.body', ['profile' => friendlyEntity($entity, false)]);
                if (
                    Notification::whereUserId($event->sentiment->message)
                                ->whereBody($message)
                                ->minutes(config('bully.minutes'))
                                ->exists()
                ) {
                    return false;
                }

                if (
                $this
                    ->sentimentRepository
                    ->isBeingBullied(
                        $entity,
                        $event->sentiment->message->team_id
                    )
                ) {
                    $this
                        ->notificationRepository
                        ->create(
                            $event->sentiment->message->team->owner,
                            [
                                'icon' => 'fa-frown-o',
                                'body' => $message,
                                'action_text' => 'View',
                                'action_url' => '/settings#/subscription',
                            ]);
                }
            });
    }
}
