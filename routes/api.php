<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {

    Route::post('/messages')
         ->uses(MessageController::class . '@store')
         ->name('api.messages.store');

    Route::post('/sentiments')
         ->uses(SentimentController::class . '@storeOrUpdate')
         ->name('api.sentiments.storeOrUpdate');

    Route::post('/profiles')
         ->uses(ProfileController::class . '@storeOrUpdate')
         ->name('api.profiles.storeOrUpdate');

    Route::get('/profiles/{id}')
         ->uses(ProfileController::class . '@show')
         ->name('api.profiles.show');

});
