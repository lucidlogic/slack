<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show')->name('home');

Route::get('privacy', 'HomeController@privacy')->name('privacy');

Route::group(['middleware' => 'auth', 'prefix' => 'dashboard'], function () {

    Route::get('/')
         ->uses(DashboardController::class . '@general')
         ->name('user.dashboard.general');

    Route::get('profile/{profile}')
         ->uses(DashboardController::class . '@profile')
         ->name('user.dashboard.profile');

    Route::get('entity/{entity}')
         ->uses(DashboardController::class . '@entity')
         ->name('user.dashboard.entity');

});

Route::get('/slack/login')
     ->uses(Auth\LoginController::class . '@handleProviderCallback')
     ->name('slack.login');

Route::get('/slack/redirect')
     ->uses(Auth\LoginController::class . '@redirectToProvider')
     ->name('slack.redirect');

Route::post('/settings/subscription/paypal')
     ->uses(Settings\Subscription\PayPalController::class . '@subscribe')
     ->name('settings.subscription.paypal');

Route::get('paypal/success', ['as' => 'paypal.success', 'uses' => PayPalController::class . '@success']);
